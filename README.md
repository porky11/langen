# Language generator

This language generator can generate lists of words using specified rules.

These rules are specified in multiple files in the same directory, which needs to be set as argument when running the generator.

The genrator outputs the langauge in markdown format, sorted by word kind.

```
# Word Kind 1

* `word 1`: meaning 2
* `word 2`: meaning 2
* ...

# Word Kind 2

* ...

...
```

# Files

These files are used for the language generation:
* `sets.kv`
* `combinations.kv`
* `*.ln` for each word kind

You can see an example setup in the `test` directory.

## Sets

In the file `sets.kv`, the set of letters are defined.
It consists of multiple lines of this format:

```
K: abc
```

In this case, `K` is a single letter identifier of a letter set.
A letter set can be used to group similar letters (here `abc`) together (for example vowels).

## Combinations

In the file `combinations.kv`, the rules for each word kind are specified.
This can be used for grouping words based on your own wishes.

For example you could define some rules for important words being short and easy to pronounce, and rules for less important words being longer or more difficult to pronounce.
You could also define some word kinds to always begin or end with a specific set of characters.

The file has multiple lines and looks like this:
```
Word Kind 1: A
Word Kind 2: AB|ABC
```

First you specify the name of the word kind, then you specify one or multiple rules to build words of this word kind, separated by a `|`.

## Word lists

For each word kind, you can define a word list called `<Word Kind>.ln`.

The file consists of multiple lines, each being a translation/description for the word.

If want some words to have a specific meaning, add the word before the translation, separated by a `:`.

## Testing

The easiest way to build a language is by using the `test` directory:

```
cargo run -- test | pandoc -o Test.pdf && evince Test.pdf
```

This first builds the test language, then converts it to a pdf and opens it.
You need to have `pandoc` and `evince` installed. Feel free to use a different document conversion tool or pdf viewer.

You might also run this command, if you just want to get a markdown file:

```
cargo run -- test > test.md
```

## Running

After the program is installed, you can run the program using `langen <directory>`. The directory should countain a `sets.kv` and a `combinations.kv` file.
