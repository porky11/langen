use rand::seq::SliceRandom;
use rand::thread_rng;
use std::collections::HashMap;
use std::collections::hash_map::Entry;
use std::fs::File;
use std::io::{BufRead, BufReader};
use std::path::{Path, PathBuf};
use std::process::ExitCode;

#[derive(Debug)]
struct WordMeaning {
    word: String,
    meaning: String,
}

#[derive(Debug)]
struct WordList {
    word_kind: String,
    words: Vec<String>,
}

#[derive(Debug)]
struct WordMeaningList {
    word_kind: String,
    word_meanings: Vec<WordMeaning>,
}

fn read_letter_sets(directory: &Path) -> Option<HashMap<char, String>> {
    let mut result = HashMap::new();
    let file_path = directory.join("sets.kv");
    let file = File::open(file_path).ok()?;

    let reader = BufReader::new(file);
    let mut found = false;
    let mut key = '\0';
    let mut value = String::new();

    for line in reader.lines() {
        let line = line.ok()?;

        let text = line
            .split_once('#')
            .map_or(line.as_ref(), |(text, _comment)| text);

        for c in text.chars() {
            if c == ' ' {
                continue;
            }
            if c == ':' {
                found = true;
                continue;
            }

            if found {
                value.push(c);
            } else {
                if key != '\0' {
                    continue;
                }
                key = c;
            }
        }

        if !found {
            continue;
        }
        if key == '\0' {
            continue;
        }

        match result.entry(key) {
            Entry::Vacant(e) => {
                e.insert(value);
            }
            Entry::Occupied(mut o) => {
                o.get_mut().push_str(&value);
            }
        }
        found = false;
        key = '\0';
        value = String::new();
    }

    Some(result)
}

fn read_word_kinds(directory: &Path) -> Option<Vec<WordList>> {
    let mut result = Vec::new();
    let file_path = directory.join("combinations.kv");
    let file = File::open(file_path).ok()?;

    let reader = BufReader::new(file);

    for line in reader.lines() {
        let line = line.ok()?;
        let line = line.trim();

        if line.is_empty() {
            continue;
        }

        let text = line
            .split_once('#')
            .map_or(line.as_ref(), |(text, _comment)| text);

        let (word_kind, words) = text.split_once(':')?;

        let word_kind = word_kind.trim().to_string();
        let words = words
            .split('|')
            .map(|word| word.trim().to_string())
            .collect();

        result.push(WordList { word_kind, words });
    }

    Some(result)
}

fn read_meanings(filename: &PathBuf, words: &mut [String]) -> Option<Vec<WordMeaning>> {
    let mut result = Vec::new();
    let file = File::open(filename).ok()?;

    let reader = BufReader::new(file);

    let mut definition = false;
    let mut meaning = String::new();
    let mut word = String::new();
    let mut always_defined = true;
    let mut line_number = 0;
    let mut i = 0;

    for line in reader.lines() {
        let line = line.ok()?;

        let text = line
            .split_once('#')
            .map_or(line.as_ref(), |(text, _comment)| text);

        for c in text.chars() {
            if c == ':' {
                assert!(
                    always_defined,
                    "{}:{}: User defined words are required to be defined first!",
                    filename.display(),
                    line_number
                );
                definition = true;
                continue;
            }

            let line = if definition { &mut word } else { &mut meaning };

            if line.is_empty() && c == ' ' {
                continue;
            }
            line.push(c);
        }

        line_number += 1;

        if meaning.is_empty() {
            continue;
        }
        assert!(
            i < words.len(),
            "{}:{}: More words of word kind than allowed ({})!",
            filename.display(),
            line_number,
            words.len()
        );
        let index = i;
        i += 1;

        if word.is_empty() {
            assert!(
                !definition,
                "{}:{}: Expected definition after colon!\n",
                filename.display(),
                line_number
            );
            always_defined = false;
        } else {
            std::mem::swap(&mut word, &mut meaning);
            let Some(alternative_index) = words.iter().position(|w| w == &word) else {
                panic!(
                    "{}:{}: User defined word does not match the rules for its word kind!",
                    filename.display(),
                    line_number
                );
            };
            words.swap(index, alternative_index);
        }

        result.push(WordMeaning {
            word: words[index].clone(),
            meaning,
        });

        definition = false;
        meaning = String::new();
        word = String::new();
    }

    Some(result)
}

fn to_words(letter_combinations: &[String], letter_sets: &HashMap<char, String>) -> Vec<String> {
    let mut result = Vec::new();

    for letter_combination in letter_combinations {
        let size = letter_combination.len();
        let mut indices = vec![0; size];
        let mut current_letter_sets = Vec::with_capacity(size);

        for c in letter_combination.chars() {
            if let Some(set) = letter_sets.get(&c) {
                current_letter_sets.push(set.clone());
            } else {
                current_letter_sets.push(String::new());
            }
        }

        loop {
            let mut increase_next = true;
            let mut current = String::new();

            for i in 0..size {
                current.push(
                    current_letter_sets[i]
                        .chars()
                        .nth(indices[i])
                        .expect("This should exist"),
                );
                if increase_next {
                    if indices[i] + 1 < current_letter_sets[i].len() {
                        indices[i] += 1;
                        increase_next = false;
                    } else {
                        indices[i] = 0;
                    }
                }
            }

            result.push(current);

            if increase_next {
                break;
            }
        }
    }

    result
}

fn main() -> ExitCode {
    let mut args = std::env::args();
    args.next();

    let Some(dir) = args.next() else {
        eprintln!("No directory specified!");
        return ExitCode::FAILURE;
    };

    let dir = dir.as_ref();

    let Some(letter_sets) = read_letter_sets(dir) else {
        eprintln!("Error parsing letter sets!");
        return ExitCode::FAILURE;
    };

    let Some(word_lists) = read_word_kinds(dir) else {
        eprintln!("Error parsing word kinds!");
        return ExitCode::FAILURE;
    };

    let mut word_meaning_lists = Vec::new();

    for word_list in word_lists {
        let mut words = to_words(&word_list.words, &letter_sets);
        let mut rng = thread_rng();
        words.shuffle(&mut rng);

        let Some(word_meanings) = read_meanings(
            &dir.join(word_list.word_kind.clone()).with_extension("ln"),
            &mut words,
        ) else {
            eprintln!("Error parsing word lists!");
            return ExitCode::FAILURE;
        };

        let word_meaning_list = WordMeaningList {
            word_kind: word_list.word_kind,
            word_meanings,
        };

        word_meaning_lists.push(word_meaning_list);
    }

    for word_meaning_list in word_meaning_lists {
        println!("# {}", word_meaning_list.word_kind);
        println!();

        for word_meaning in word_meaning_list.word_meanings {
            println!("* `{}`: {}", word_meaning.word, word_meaning.meaning);
        }

        println!();
    }

    ExitCode::SUCCESS
}
